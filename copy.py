import tkinter as tk
import pyperclip
import glob
import os


root = tk.Tk()
root.title('Copy Text')


text_copy = dict()
def key(event):
    global text_copy
    try:
        text_key = int(event.char)
    except:
        return
    if 0 <= text_key <= 9:
        pyperclip.copy(text_copy[text_key])
root.bind('<Key>', key)


def copy_text(text):
    pyperclip.copy(text)


path = 'CopyTextDirectory'
i = 0
for filename in glob.glob(os.path.join(path, '*.txt')):
    with open(os.path.join(os.getcwd(), filename), 'r') as f:
        text = ''.join(f.readlines())
        btn = tk.Button(anchor='w', text='{} {}'.format(i, filename.replace('.txt', '').replace('CopyTextDirectory/', '')), command=lambda text=text: copy_text(text=text))
        text_copy[i] = text
        i = i + 1
        btn.pack(fill='both')

root.mainloop()

